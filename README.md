Guideline on using systemd to autostart scripts on the Raspberry Pi.

Step1: Create systemd service file by typing the below command in raspberry pi os:

In general:sudo nano /etc/systemd/system/name-of-your-service.service

Ex: sudo nano /etc/systemd/system/autostart.service

Enter the below content in service file and edit the ExecStart and working directory path like  : “/home/pi/{file path which need to run}”

[Unit]
Description=name-of-your-service Service
After=network.target

[Service]
Type=idle
User=pi
ExecStart=/usr/bin/python3 /home/pi/Documents/app.py
WorkingDirectory=/home/pi/Documents/
Restart=always

[Install]
WantedBy=multi-user.target

save the file with CTRL+O and close the editor with with CTRL+X.

Step2: Change file permissions
Make file permission to readable by typing the below command:

General:
sudo chmod 644 /etc/systemd/system/name-of-your-service.service

Ex: sudo chmod 644 /etc/systemd/system/autostart.service


Step3: Inform the system
As the last step, you need to tell the system that you have added this file and want to enable this service so that it starts at boot.

sudo systemctl daemon-reload

To start the service at bootup enter the command

sudo systemctl enable autostart.service

Reboot your Pi, and you are all set!


Reference:autorun a script whenever a pi bootup.

